﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Bullet : MonoBehaviour
{
    [SerializeField] float bulletSpeed = 5.0f;
    [SerializeField] float lifeTime = 5.0f;
    [SerializeField] int damage = 100;

    [SerializeField] int pullingForce = 200;
    private Vector2 direction = Vector2.right;

    private bool playerOrientation =true;
    private GameObject playerReference;

    Rigidbody2D rb;


    void Start()
    {
        
        //gets the reference to the rigidbody
        rb = GetComponent<Rigidbody2D>();

        //sets the lifetime of the bullet
        Destroy(gameObject, lifeTime);

    }

    //checks if is suppose to shoot based on the orientation of the player
    public void setDirection(Vector2 pDirection){

        direction = pDirection;
        //shows the bullet sprite
        GetComponent<SpriteRenderer>().enabled = true;
    }

    private void FixedUpdate()
    {
        //sets the velocity of the bullet
        rb.velocity = direction * bulletSpeed;
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
         //if it encounter an enemy does damage
        if(other.transform.tag.Equals("FlyingEnemy")){

            other.gameObject.GetComponent<Rigidbody2D>().AddForce(rb.velocity*-pullingForce);

            //does damage to the enemy
            int health = other.gameObject.GetComponent<EnemyHealthManager>().Damage(damage);

            //if the enemy health is 0 then updates the healtmanager
            if(health==0){
                playerReference.GetComponent<HealthManager>().enemyKilled();
            }
            
        }

        //destroy when it touches something
        Destroy(gameObject);
    }
}
