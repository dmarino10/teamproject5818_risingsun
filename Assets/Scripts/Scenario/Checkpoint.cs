﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    [SerializeField]
    private GameObject player;

    [SerializeField]
    private GameObject checkpoint;

   public void Teleport()
   {
       //Debug.Log("worked");
       player.transform.position = checkpoint.transform.position;
   }


}
