﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordController : MonoBehaviour
{
    [SerializeField] int damage = 200;

    void OnTriggerEnter2D(Collider2D other)
    {

        //if it encounter an enemy does damage
        if (other.transform.tag.Equals("FlyingEnemy"))
        {
            //does damage to the enemy

            int health = other.gameObject.GetComponent<EnemyHealthManager>().Damage(damage);
            //if the enemy health is 0 then updates the healtmanager
            if (health == 0)
            {
                transform.parent.GetComponent<HealthManager>().enemyKilled();
            }

        }

    }
}
