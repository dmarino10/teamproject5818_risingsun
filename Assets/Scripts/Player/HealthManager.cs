﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class HealthManager : MonoBehaviour
{
    // Health Manager Singleton
    public static HealthManager playerIns;
    
    //Property is used here to ensure player health never drops below zero or goes below max health
    [SerializeField] private int maxHealth = 500;

    [SerializeField] public Slider slider;

    public int MaxHealth{  get { return maxHealth;}}

    [SerializeField]
    private Material material; 
    [SerializeField]
    private Color originalTint;
    [SerializeField]
    private float tintFadeSpeed = 1f; 

    [SerializeField]
    ManageScenes sceneManager;  

    // Toggle invincible state
    private bool isInvincible = false; 

    public bool IsInvincible { get => isInvincible; set => isInvincible = value; }
    
    private int playerHealth;
    public int PlayerHealth 
    {
        get
        {
            return playerHealth;
        }
        set
        {
            if (value < 0)
            {
                playerHealth = 0;
            }
            else if (value > playerIns.MaxHealth)
            {
                playerHealth = playerIns.MaxHealth;
            }
            else
            {
                playerHealth = value;
            }
        }
    }

    //Health Recovery Variables
    bool healRecStarted = false;
    public int killedEnemies = 0;
    [SerializeField] float healRecCycle = 5f;
    [SerializeField] private static int[] recoveryTiers = 
    {
        -50,
        -100,
        -200,
        -250
    };

    //Singleton created
    private void Awake()
    {
        slider.maxValue = maxHealth;

        if (playerIns == null) 
        {
            playerIns = this;
        }
        playerIns.PlayerHealth = playerIns.MaxHealth;

        //material = transform.Find("Body").GetComponent<MeshRenderer>().material;
    }

    //Function to deal damage to player
    public void Damage(int dmgDealt)
    {
        // Activate shader 
        if (dmgDealt>0 && !isInvincible)
        {
            // Red color when hit
            //source.Play();
            SetTintColor(new Color(1, 0, 0, 1f));         
            //Just use negative damage to heal
            playerIns.PlayerHealth = playerIns.PlayerHealth - dmgDealt;           
        }

        if (dmgDealt<0)
        {
            // Green color when healed
            SetTintColor(new Color(0, 1 , 0, 1f));                 
            playerIns.PlayerHealth = playerIns.PlayerHealth - dmgDealt;
        }

        //Starts health recovery system
        if (!healRecStarted)
        {
            healRecStarted = true;
            StartCoroutine(recoveryCycle());
        }
    }

    IEnumerator recoveryCycle ()
    {
        yield return new WaitForSeconds(healRecCycle);

        //Heals based on enemies killed
        switch(killedEnemies)
        {
            case 0:
                playerIns.Damage(0);
                break;
            case 1:
                playerIns.Damage(recoveryTiers[killedEnemies-1]);
                break;
            case 2:
                playerIns.Damage(recoveryTiers[killedEnemies-1]);
                break;
            case 3:
                playerIns.Damage(recoveryTiers[killedEnemies-1]);
                break;
            default:
                playerIns.Damage(recoveryTiers[3]);
                break;
        }

        //Restarts system
        healRecStarted = false;
        killedEnemies = 0;
    }

    private void Update()
    {
        if(playerIns.PlayerHealth==0){

            //handles the game over for now it just goes back to the initial point
            //gameObject.GetComponent<PlayerController>().BackToBeginning();
            sceneManager.GameOver();
            playerIns.PlayerHealth = maxHealth;
        }

        slider.value = playerHealth;

        // Restore normal color 

        material.SetColor("_Tint", originalTint); 
        if (originalTint.a > 0)
        {
            originalTint.a = Mathf.Clamp01(originalTint.a - tintFadeSpeed * Time.deltaTime);
            material.SetColor("_Tint", originalTint);

        }

    }

    public void SetTintColor(Color color) 
    {
        originalTint = color;
        material.SetColor("_Tint", originalTint);
    }

    public void instaKill(){
        playerIns.PlayerHealth=0;
    }

    public void enemyKilled(){
        killedEnemies++;
    }
}
