﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    //the player
    public Transform Player;
    

    //the min height and max height the camara should reach (end of level)
    [SerializeField] float minHeight = 0.33f, maxHeight = 3.49f;

    //the min width and max width of the camara
    [SerializeField] float minWidth = -0.11f, maxWidht = 3.0f;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float y = Mathf.Clamp (Player.position.y, minHeight, maxHeight);   
        float x = Mathf.Clamp (Player.position.x, minWidth, maxWidht);    
        gameObject.transform.position = new Vector3 (x, y,gameObject.transform.position.z);

 
    
    }
}
