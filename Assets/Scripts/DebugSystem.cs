﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class DebugSystem : MonoBehaviour
{
    [SerializeField]
    private Canvas debugMenu;

    [SerializeField]
    private PlayerController player;

    [SerializeField]
    private GameObject checkpoints; 

    [SerializeField]
    private GameObject flyingEnemy;

    [SerializeField]
    private GameObject normalEnemy;

    [SerializeField]
    private GameObject[] enemies = new GameObject[8]; 
    //private List<GameObject> enemies = new List<GameObject>(); 

    [SerializeField]
    private bool[] enemyAlive = new bool [8];

    [SerializeField]
    private Vector3[] enemyPosition = new Vector3 [8];

    [SerializeField]
    private TextMeshProUGUI speedText;

    [SerializeField]
    private TextMeshProUGUI jumpText;

    [SerializeField]
    private TextMeshProUGUI blinkText;

    [SerializeField]
    private TextMeshProUGUI debugText;
    private bool activeCheckpoints = false;

    private bool debugMode = false;

    [SerializeField]
    private bool flyingEnemyAlive, normalEnemyAlive;

    private Vector3 flyingPos;
    private Vector3 enemyPos;

    [SerializeField]
    private ManageScenes sceneManager;

    void Start()
    {
        flyingEnemyAlive = true;
        normalEnemyAlive = true;

        for (int i = 0; i < enemies.Length; i++)
        {
            enemyPosition[i] = enemies[i].transform.position;
        }
        
        //flyingPos = enemies[1].transform.position;
        //enemyPos =  enemies[0].transform.position;                       
    }   

    void Update()
    {
        // Activate debug mode. 
        if(Input.GetKeyDown(KeyCode.Alpha0))
        {
            debugMode = !debugMode;
            debugMenu.gameObject.SetActive(debugMode);
            if (debugMode)
            {
                debugText.text = "Press 0 to leave debug mode";
                player.GameIsPaused = true;
            }
            else 
            {
                debugText.text = "Press 0 to enter debug mode";
                player.GameIsPaused = false;
            }
        }

        // End the game
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
            //sceneManager.GameEnd();
        }
    }

    // Functions to be accessed using OnClick. 
    public void ReceiveDamage()
    {
        HealthManager.playerIns.Damage(20);
    }

    public void Heal()
    {
        HealthManager.playerIns.Damage(-20);
    }

    public void FullHealth()
    {
        HealthManager.playerIns.Damage(-HealthManager.playerIns.MaxHealth);
    }

    public void Die()
    {
        HealthManager.playerIns.Damage(HealthManager.playerIns.MaxHealth);
        sceneManager.GameOver();
    }

    public void SpawnEnemies()
    {
        for (int i = 0; i < enemies.Length; i++)
        {
            if (enemies[i]==null && i < 6)
            {
                enemies[i]=(Instantiate(normalEnemy, enemyPosition[i], Quaternion.identity));
                enemyAlive[i] = true;
            }
            
            if (enemies[i]==null && i >= 6)
            {
                enemies[i]=(Instantiate(flyingEnemy, enemyPosition[i], Quaternion.identity));
                enemyAlive[i] = true;
            }                    
        }
        /*
        if (enemies[1]==null)
        {
            enemies[1]=(Instantiate(flyingEnemy, flyingPos, Quaternion.identity));
            enemyAlive[1] = true;
            //flyingEnemyAlive = true;

        }
        if(enemies[0]==null)
        {
            enemies[0]=(Instantiate(normalEnemy, enemyPos, Quaternion.identity));
            enemyAlive[1] = true;
            //normalEnemyAlive = true;
        }*/
    }

    public void KillAll()
    {
        //Kill all enemies
        if(enemies.Length>0)//enemies.Count>0)
        {
            for (int i = 0; i < enemies.Length; i++)
            {
                Destroy(enemies[i]);
                enemyAlive[i] = false;
            }
            /*
            foreach (GameObject enemy in enemies)
            {
                Destroy(enemy);
            }*/

            System.Array.Clear(enemies,0,enemies.Length);
            //enemies.Clear();
        }
        //flyingEnemyAlive = false;
        //normalEnemyAlive = false;
    }

    public void InvincibleMode()
    {
        player.ActivateForceField();
    }

    public void ShowCheckpoints()
    {
        activeCheckpoints = !activeCheckpoints;
        checkpoints.SetActive(activeCheckpoints);
    }

    public void PlayerSpeed(float newSpeed)
    {
        player.MoveSpeed=newSpeed; //Change Player Speed
        speedText.text = "Player Speed: " + newSpeed;

    }

    public void PlayerJumpHeight(float newJumpForce)
    {
        player.JumpForce = newJumpForce; //Change jump height
        jumpText.text = "Jumpe force: " + newJumpForce;
    }

    public void PlayerBlinkDistance(float newDistance)
    {
        player.BlinkDistance = newDistance; //Change Player Blink Distance
        blinkText.text = "Blink Distance: " + newDistance;
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Checkpoint01()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Checkpoint02()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Checkpoint03()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }


}
