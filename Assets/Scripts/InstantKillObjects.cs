﻿
using UnityEngine;

public class InstantKillObjects : MonoBehaviour
{

	 private void OnTriggerEnter2D(Collider2D other)
    {
		if(other.gameObject.tag.Equals("Player")){
			
			other.gameObject.GetComponent<HealthManager>().instaKill();
		}
    }
}
