﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(HealthManager))]
[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour
{
    //parameters that can be change that have to do with speed and force
    [SerializeField] float moveSpeed = 5.0f;
    [SerializeField] float jumpForce = 20.0f;
    [SerializeField] float blinkDistance = 5.0f;

    //parameters to help with the jumping
    public LayerMask whatIsGround;
    public Transform groundReference;
    private bool canDoubleJump;


    //parameters of the force field
    [SerializeField]
    private GameObject forceField;
    public bool field;


    //parameters of the gun
    public Transform gunReference;
    public GameObject bulletPrefab;


    //parameters of the restart
    private Vector3 origin;

    //references to the components of the player
    Rigidbody2D rb;
    SpriteRenderer sr;
    Animator animator;

    // Reference to health manager
    [SerializeField]
    HealthManager healthManager;

    // 
    private bool gameIsPaused;

    public float MoveSpeed { get => moveSpeed; set => moveSpeed = value; }
    public float JumpForce { get => jumpForce; set => jumpForce = value; }
    public float BlinkDistance { get => blinkDistance; set => blinkDistance = value; }
    public bool GameIsPaused { get => gameIsPaused; set => gameIsPaused = value; }

    void Start()
    {
        //get reference at start point
        origin = transform.position;

        // at start i get all the references
        rb = gameObject.GetComponent<Rigidbody2D>();
        sr = gameObject.GetComponent<SpriteRenderer>();
        animator = gameObject.GetComponent<Animator>();

        //starts without field
        field = false;
    }

    void Update()
    {
        if (!gameIsPaused)
        {
            //changing the velocity of the player deppending on the input in the horizontal
            float xVel = moveSpeed * Input.GetAxis("Horizontal");
            float yVel = rb.velocity.y;

            rb.velocity = new Vector2(xVel, yVel);

            // now i have to see if the player is in the ground
            bool isInGround = Physics2D.OverlapCircle(groundReference.position, .2f, whatIsGround);
            if (isInGround)
            {
                canDoubleJump = true;
            }

            //now i manage the jumping
            if (Input.GetButtonDown("Jump"))
            {
                if (isInGround)
                {
                    //if its in the ground i jump
                    rb.velocity = new Vector2(rb.velocity.x, jumpForce);
                }
                else if (canDoubleJump)
                {
                    //else if i can double jump i double jump
                    rb.velocity = new Vector2(rb.velocity.x, jumpForce);
                    canDoubleJump = false;
                }

            }

            //manages the force fiel
            //! I Changed the input because we are going to use mouse click on the Debug System
            //cool, next time tho, you should just change it in the project settings
            if (Input.GetButtonDown("Shield"))
            {
                ActivateForceField();
            }

            //manages the blink option
            if (Input.GetButtonDown("Blink"))
            {
                ManagesBlink();
            }

            //manages the sword 
            if (Input.GetButtonDown("Sword"))
            {
                SwingSword();
            }

            if (Input.GetButtonDown("Gun"))
            {
                Shoot(false);
            }
            if (Input.GetButtonDown("GunController"))
            {
                Shoot(true);
            }

            //change the player orientation depending on where is going
            if (rb.velocity.x < 0) transform.rotation = Quaternion.Euler(0, 180f, 0);
            else if (rb.velocity.x > 0) transform.rotation = Quaternion.Euler(0, 0, 0);
        }    
    }

    private void Shoot(bool controller)
    {
        if (AmmoManager.playerIns.PlayerAmmo != 0)
        {
            Vector2 direction = giveShootingDirection(controller);
			turningOnShooting(direction);

            GameObject bullet = Instantiate(bulletPrefab, gunReference.position, Quaternion.identity) as GameObject;
            bullet.GetComponent<Bullet>().setDirection(direction);
            AmmoManager.playerIns.AmmoUpdate(-1);
        }
    }

    private Vector2 giveShootingDirection(bool controller)
    {
		Vector2 direction = Vector2.right;

        if (!controller)
        {
            //gets the mouse position and sets is as the destination
            Ray castPoint = Camera.main.ScreenPointToRay(Input.mousePosition);
			direction = (castPoint.origin - gameObject.transform.position);

        }
		else{
			
            float x = Input.GetAxis("AimHorizontal");
            if(x!=0){direction.x =x;}  

            float y = Input.GetAxis("AimVertical");
            if(y!=0){direction.y = y;}

		}

		direction.Normalize();

        return direction;
    }

	private void turningOnShooting(Vector2 direction){

		if (direction.x < 0) transform.rotation = Quaternion.Euler(0, 180f, 0);
        else if (direction.x > 0) transform.rotation = Quaternion.Euler(0, 0, 0);
	}

    private void SwingSword()
    {
        animator.SetTrigger("Swing");
    }

    public void ActivateForceField()
    {
        field = !field;
        forceField.SetActive(field);
    }

    private void ManagesBlink()
    {
        healthManager.SetTintColor(new Color(1, 1, 1, 1f));

        //asumme that the player is facing right
        Vector2 direction = Vector2.right;

        //if the player is facing left then change the direction to left
        if (transform.rotation.y == -1)
        {
            direction = Vector2.left;
        }

        //here i have to check whats happening in the new position
        RaycastHit2D check = Physics2D.Raycast(transform.position, direction, blinkDistance);

        //if it hits something 
        if (check.collider != null)
        {
            if (check.transform.tag.Equals("Ground"))
            {
                //if i'm tring to go throug the tile map of the level then i go out of the method 
                return;
            }
        }

        //creates the new position
        Vector2 newPosition = transform.position;
        newPosition += direction * blinkDistance;

        //if there is nothing in the position i'm going then i can change my position
        if (!Physics2D.OverlapCircle(newPosition, (sr.bounds.size.x / 2)))
        {
            transform.position = newPosition;
        }

    }

    public void WasHit()
    {
        // Once the player was hit, we can access their HP and other stats. 
        Debug.Log("Was hit!");
    }

    public void BackToBeginning()
    {

        transform.position = origin;
    }

}
