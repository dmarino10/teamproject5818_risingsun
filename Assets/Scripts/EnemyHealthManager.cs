﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealthManager : MonoBehaviour
{
    
    //Property is used here to ensure enemy health never drops below zero or goes below max health
    [SerializeField] private int maxEnHealth = 300;
    [SerializeField] private int timeDamageOnScreen = 5;
    public int MaxEnHealth
    {  
        get 
        {
            return maxEnHealth;
        }
    }
    private int enemyHealth;
    public int EnemyHealth 
    {
        get
        {
            return enemyHealth;
        }
        set
        {
            if (value < 0)
            {
                enemyHealth = 0;
            }
            else if (value > MaxEnHealth)
            {
                enemyHealth = MaxEnHealth;
            }
            else
            {
                enemyHealth = value;
            }
        }
    }

    [SerializeField] public Slider slider;
    [SerializeField] public Text damageText;

    //Sets health on start
    private void Awake()
    {
        EnemyHealth = MaxEnHealth;
        slider.maxValue = MaxEnHealth;
    }

    //Function to deal damage to player
    public int Damage(int dmgDealt)
    {
        //Just use negative damage to heal
        StartCoroutine(showDamage(dmgDealt));
        EnemyHealth = EnemyHealth - dmgDealt;
        return EnemyHealth;
    }

    // If health reaches zero, destroy object
    private void Update()
    {
        if (EnemyHealth == 0)
        {
            if(transform.parent){

                GameObject parent = transform.parent.gameObject;
                transform.parent = null;
                Destroy(parent);
            }
            Destroy(gameObject);
        }

        slider.value = enemyHealth;
    }

    IEnumerator showDamage(int dmg){

        for(int i=0; i < timeDamageOnScreen; i++){

            damageText.text= dmg + "";
            slider.gameObject.SetActive(true);
            yield return new WaitForSeconds(1);
        }

        damageText.text="";
        slider.gameObject.SetActive(false);
    }
}
