﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyEnProjectile : MonoBehaviour
{
    [SerializeField] int thisGuyDmg = 150;
    
    private void OnCollisionEnter2D(Collision2D other)
    {
        //Deal damage if it heals player
        if (other.gameObject.tag == "Player")
        {
            HealthManager.playerIns.Damage(thisGuyDmg);
            Destroy(gameObject);
        }
    }

    void Die()
    {
        Destroy(gameObject);
    }

    private void Start()
    {
        Invoke("Die", 2f);
    }
}
