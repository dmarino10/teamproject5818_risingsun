﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[RequireComponent (typeof (Rigidbody2D))]
public class BaseEnemyVariant : MonoBehaviour
{
    Rigidbody2D rb;
	//SpriteRenderer sr;
    [SerializeField] GameObject rightWP;
    [SerializeField] GameObject leftWP;

    public float baseVariantEnemy_MoveSpeed = 4.5f;
    protected int direction;
    [SerializeField] int thisGuyDmg = 100;

    protected Animator animator;
    private SpriteRenderer sr;
 
    void Awake()
    {
		//For now, there is no sprite to be rendered, but let's leave it here for now
        //sr = gameObject.GetComponent<SpriteRenderer>();
		rb = gameObject.GetComponent<Rigidbody2D>();
        animator = gameObject.GetComponent<Animator>();
        sr = gameObject.GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        direction = Random.Range(0, 2)*2-1;
    }

    protected void Update()
    {
        if (direction == 1)
        {
            if ( Vector2.Distance(rightWP.transform.position, gameObject.transform.position) <= 2.5f) 
            {
                ChangeDirection();
            }
        }
        if (direction == -1)
        {
            if ( Vector2.Distance(leftWP.transform.position, gameObject.transform.position) <= 2.5f) 
            {
                ChangeDirection();
            }
        }
        
    }

    private void FixedUpdate()
    {
         rb.velocity = new Vector2(baseVariantEnemy_MoveSpeed*direction, rb.velocity.y);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            animator.SetTrigger("Swing");
            HealthManager.playerIns.Damage(thisGuyDmg);
            ChangeDirection();
        }
    }

    private void ChangeDirection()
    {
        direction = direction * -1;
		//change the player orientation depending on where is going
		if (rb.velocity.x < 0) sr.flipX=true;
		else if (rb.velocity.x > 0) sr.flipX=false;
    }
}
