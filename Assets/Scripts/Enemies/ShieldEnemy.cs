﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ShieldEnemy : BaseEnemyVariant
{
    
    
    [SerializeField] float distanceBeforeShield = 7.0f;
    [SerializeField] float distanceBeforeAttack = 0.5f;

    [SerializeField] GameObject shield;

    private bool shieldUp;

    void Update()
    {
        //calls parent update
        base.Update();


        //sets the direction the enemy is lookig at
        Vector2 looking = Vector2.right;
        if(direction==-1) looking = -looking;

        RaycastHit2D hit = Physics2D.Raycast(transform.position, looking);
        if (hit.collider != null){
            
            if(hit.collider.gameObject.tag.Equals("Player")){

                //if(hit.distance <= distanceBeforeAttack){

                //}
                //else 
                if(hit.distance <= distanceBeforeShield){
                    
                    shieldUp = true;
                }
            }
            else{
                shieldUp = false;            
            }


        }

        animator.SetBool("Defend", shieldUp);
    }
}
