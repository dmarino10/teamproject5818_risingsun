﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingEnemyController : MonoBehaviour
{
    //Here are all the variables, most are public so they can be acessed by the animator
    [SerializeField] GameObject player;
    [SerializeField] FlyEnProjectile projectile;
    [SerializeField] GameObject rightEnd;
    [SerializeField] GameObject leftEnd;
    Animator anim;
    public GameObject[] waypoints = new GameObject[4];
    public GameObject core;
    [SerializeField] float baseMoveSpeed = 4.7f;
    public float moveSpeed;
    public int fleeModifier = 3;
    [SerializeField] float projectileSpeed = 5f;
    public Rigidbody2D rb;

    //Gets the objects rigid body and animator
    void Awake()
    {
		rb = gameObject.GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        moveSpeed = baseMoveSpeed;
        player = GameObject.FindGameObjectsWithTag("Player")[0];
    }

    //Manages the distance to player, which act as condition to change behaviour
    private void Update()
    {
        anim.SetFloat("distanceToPlayer", Vector2.Distance(core.transform.position, player.transform.position));
    }

    //destroy waypoints when enemy die to avoid clutter
    private void OnDestroy()
    {
        for(int i = 0; i < waypoints.Length; i++)
        {
            Destroy(waypoints[i]);
        }
    }

    public GameObject GetPlayer()
    {
        return player;
    }

    //Fires a projectile at the player's direction
    public void FireRight()
    {
        GameObject b = Instantiate(projectile.gameObject, rightEnd.transform.position, Quaternion.identity);
        Vector2 direction = (player.transform.position - b.transform.position).normalized;
        b.GetComponent<Rigidbody2D>().velocity = direction * projectileSpeed;
    }

    //Fires a projectile at the player's direction
    public void FireLeft()
    {
        GameObject b = Instantiate(projectile.gameObject, leftEnd.transform.position, Quaternion.identity);
        Vector2 direction = (player.transform.position - b.transform.position).normalized;
        b.GetComponent<Rigidbody2D>().velocity = direction * projectileSpeed;
    }

    public void StopFiring()
    {
        CancelInvoke("FireRight");
        CancelInvoke("FireLeft");
    }

    public void StartFiring()
    {
        InvokeRepeating("FireRight", 0.33f, 1f);
        InvokeRepeating("FireLeft", 0.25f, 1f);
    }
}
