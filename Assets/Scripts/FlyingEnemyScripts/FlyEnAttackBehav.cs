﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyEnAttackBehav : StateMachineBehaviour
{
    FlyingEnemyController flyingEn;
    GameObject player;
    GameObject myCore;
    Vector2 positionLock;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        flyingEn = animator.gameObject.GetComponent<FlyingEnemyController>();
        player = flyingEn.GetPlayer();
        myCore = flyingEn.core;
        positionLock = flyingEn.transform.position;
        flyingEn.StartFiring();

        flyingEn.rb.velocity = new Vector2(0, 0);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        flyingEn.transform.position = positionLock;
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
       flyingEn.StopFiring();
    }
}
