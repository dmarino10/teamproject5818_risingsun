﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyEnScoutBehav : StateMachineBehaviour
{
    
    FlyingEnemyController flyingEn;
    GameObject[] myWaypoints;
    GameObject myCore;
    int currentWP;
    int previousWP;
    
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        flyingEn = animator.gameObject.GetComponent<FlyingEnemyController>();
        myWaypoints = flyingEn.waypoints;
        myCore = flyingEn.core;
        currentWP = Random.Range(0,4);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        if (Vector2.Distance(myWaypoints[currentWP].transform.position, flyingEn.transform.position) < 2.0f)
        {
            previousWP = currentWP;
            while (currentWP == previousWP)
            {   
                currentWP = Random.Range(0,4);
            }
        }
        Vector2 direction = (myWaypoints[currentWP].transform.position - myCore.transform.position).normalized;
        flyingEn.rb.velocity = direction * flyingEn.moveSpeed;
    }
}
