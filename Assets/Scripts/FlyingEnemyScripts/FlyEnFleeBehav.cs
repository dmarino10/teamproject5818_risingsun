﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyEnFleeBehav : StateMachineBehaviour
{
    FlyingEnemyController flyingEn;
    GameObject player;
    GameObject myCore;
    
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        flyingEn = animator.gameObject.GetComponent<FlyingEnemyController>();
        myCore = flyingEn.core;
        player = flyingEn.GetPlayer();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //! It's too slow for some reason, really should be faster, otherwise the game is too easy
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Vector2 direction = (player.transform.position - myCore.transform.position);
        direction.Normalize();
        flyingEn.rb.velocity = -direction * flyingEn.moveSpeed;
    }
}
