﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class AmmoManager : MonoBehaviour
{
    // Ammo Manager Singleton
    public static AmmoManager playerIns;
    
    //Property is used here to ensure player ammo never drops below zero or goes below max ammo
    [SerializeField] private int maxAmmo = 10;
    [SerializeField] private Text ammoTx;

    public int MaxAmmo{  get { return maxAmmo;}}

    private int playerAmmo;
    public int PlayerAmmo 
    {
        get
        {
            return playerAmmo;
        }
        set
        {
            if (value < 0)
            {
                playerAmmo = 0;
            }
            else if (value > playerIns.MaxAmmo)
            {
                playerAmmo = playerIns.MaxAmmo;
            }
            else
            {
                playerAmmo = value;
            }
        }
    }

    //Singleton created
    private void Awake()
    {
        if (playerIns == null) 
        {
            playerIns = this;
        }
        playerIns.PlayerAmmo = playerIns.MaxAmmo;
        ammoTx.text = "Ammo: " + playerIns.PlayerAmmo;
    }

    //Function to change ammo
    public void AmmoUpdate(int ammoQt)
    {
        playerIns.PlayerAmmo = playerIns.PlayerAmmo + ammoQt;
        ammoTx.text = "Ammo: " + playerIns.PlayerAmmo;
    }

}
