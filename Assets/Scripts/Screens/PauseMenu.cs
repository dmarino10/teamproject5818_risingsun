﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    public static bool gameIsPaused = false; 

    [SerializeField]
    private GameObject pauseMenu;

    [SerializeField]
    private PlayerController player;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            if(gameIsPaused)
            {
                ResumeGame();                
            }
            else
            {
                PauseGame();
            }        
        }
    }

    private void PauseGame()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;
        gameIsPaused = true;
        player.GameIsPaused = true; 

    }

    private void ResumeGame()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        gameIsPaused = false;
        player.GameIsPaused = false;
        
    }
}
