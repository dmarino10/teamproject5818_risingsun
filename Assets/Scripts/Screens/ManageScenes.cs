﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ManageScenes : MonoBehaviour
{
    private bool gameOver; 
    private bool changeScene;
    
    [SerializeField]
    private GameObject gameOverText, gameEndText; 

    private int count = 0;

    public void StartGame()
    {       
        SceneManager.LoadScene(1);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void GameOver()
    {
        //gameOver = true;
        SceneManager.LoadScene(2);
    }   

    public void GameEnd()
    {
        //gameOver = false;
        SceneManager.LoadScene(3);
    }

    private void Update()
    {
        if (SceneManager.GetActiveScene().buildIndex == 2 || SceneManager.GetActiveScene().buildIndex == 3)
        {
            count++;

            if (count == 600)
            {
                count = 0;
                SceneManager.LoadScene(0);
            }
        }
    }
}
