﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    private int toggleTime = 0;

    // Set timer to flicker. 
    [SerializeField]
    private int laserTime = 12;

    // Types of Lase
    [SerializeField]
    private bool constantLaser = false;

    [SerializeField]
    private bool movingLaser = false;

    // Speed for a moving laser. 
    [SerializeField]
    private float laserSpeed = 10f;

    private Vector2 initialPosition;

    // Moving laser range [-movementRange, movementRange]. There must be a floor or wall in that range. 
    [SerializeField]
    private int movementRange = 5;

    private LineRenderer lr;

    void Start()
    {
        lr = GetComponent<LineRenderer>();
        initialPosition = transform.position;
    }

    void Update()
    {
        toggleTime++;

        // toggle time turns on/off the laser 
        // If laser leaves its range, it will start to move on the opposite direction. 
        if (movingLaser)
        {
            if ((transform.position.x - initialPosition.x) >= movementRange || (initialPosition.x - transform.position.x) >= movementRange)
            {
                laserSpeed *= -1;
            }
            transform.Translate(laserSpeed / 1000, 0, 0);
        }

        if (toggleTime > laserTime * 10 && !constantLaser)
        {
            toggleTime = 0;
            lr.enabled = !lr.enabled;
        }

        // If it is a constant laser, it won't flicker. 
        if (constantLaser)
        {
            lr.enabled = true;
        }

        if (lr.enabled)
        {

            lr.SetPosition(0, transform.position);


            RaycastHit2D hit = Physics2D.Raycast(transform.position, -Vector2.up);

            //if (Physics2D.Raycast(transform.position, -Vector2.up))
            //{
            if (hit.collider)
            {
                lr.SetPosition(1, hit.point);
                if (hit.transform.tag.Equals("Player"))
                {
                    Debug.Log(hit.transform.tag);

                    // Check if force field is on. 
                    if (!hit.transform.GetComponent<PlayerController>().field) 
                    {
                        HealthManager.playerIns.Damage(HealthManager.playerIns.MaxHealth);
                    }
                }
            }
            //}
            else lr.SetPosition(0, -Vector2.up);
        }
    }
}
