﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseEnScout : MonoBehaviour
{
    Rigidbody2D rb;
	//SpriteRenderer sr;
    [SerializeField] GameObject rightP;
    [SerializeField] GameObject leftP;

    public float baseEnemy_MoveSpeed = 4.5f;
    private int direction;
    private bool groundBelow = true;
    [SerializeField] int thisGuyDmg = 100;
 
    void Awake()
    {
		//For now, there is no sprite to be rendered, but let's leave it here for now
        //sr = gameObject.GetComponent<SpriteRenderer>();
		rb = gameObject.GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        direction = Random.Range(0, 2)*2-1;
    }

    private void Update()
    {
        if (groundBelow)
        {
            rb.velocity = new Vector2(baseEnemy_MoveSpeed*direction, rb.velocity.y);
        }
        if (direction == 1)
        {
            RaycastHit2D rHit = Physics2D.Raycast(rightP.transform.position, -Vector2.up, 1.0f);
            if (rHit.collider == null)
            {
                ChangeDirection();
            }
        } 
        else if (direction == -1)
        {
            RaycastHit2D lHit = Physics2D.Raycast(leftP.transform.position, -Vector2.up, 1.0f);
            if (lHit.collider == null)
            {
                ChangeDirection();
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            HealthManager.playerIns.Damage(thisGuyDmg);
            ChangeDirection();
        }
        else if (other.gameObject.tag == "Ground")
        {
            ChangeDirection();
        }
    }

    private void ChangeDirection()
    {
        direction = direction * -1;
        groundBelow = true;
        //this will also flip the sprite when there is one to do so
        //if (rb.velocity.x < 0) sr.flipX = true;
		//else if (rb.velocity.x > 0) sr.flipX = false;
    }

}
