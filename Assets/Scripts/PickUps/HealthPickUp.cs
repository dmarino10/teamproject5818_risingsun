﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickUp : PickUpBase
{
    void Start()
    {
        pickUpValue = -50;
    }

    override protected void PickedUp()
    {
        HealthManager.playerIns.Damage(pickUpValue);
    }
}
