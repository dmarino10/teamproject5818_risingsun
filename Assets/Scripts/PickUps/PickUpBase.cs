﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpBase : MonoBehaviour
{
    [SerializeField] protected int pickUpValue;

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            PickedUp();
            Destroy(gameObject);
        }
    }
    
    protected virtual void PickedUp()
    {

    }
}
