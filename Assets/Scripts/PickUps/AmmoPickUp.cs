﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoPickUp : PickUpBase
{
    void Start()
    {
        pickUpValue = 1;
    }

    override protected void PickedUp()
    {
        AmmoManager.playerIns.AmmoUpdate(pickUpValue);
    }
}
